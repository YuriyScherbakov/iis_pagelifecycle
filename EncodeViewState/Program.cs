﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EncodeViewState
{
    class Program
    {
        static void Main(string [] args)
        {

            System.Web.UI.LosFormatter formatter = new System.Web.UI.LosFormatter();
            Console.WriteLine("Вставьте строку __viewstate сюда");
            string str1 = Console.ReadLine();

            Console.WriteLine(System.Text.Encoding.ASCII.GetString(Convert.FromBase64String(str1)));
            Console.WriteLine();
        }
    }
}
