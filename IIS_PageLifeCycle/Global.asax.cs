﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace IIS_PageLifeCycle
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            
        }
        protected void Application_Error(object sender,EventArgs e)
        {
            //Здесь еще можно отловить ошибку
            this.Server.ClearError();
        }
    }
}