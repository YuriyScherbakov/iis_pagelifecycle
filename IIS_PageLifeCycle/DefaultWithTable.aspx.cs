﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IIS_PageLifeCycle
{
    public partial class MainForm : System.Web.UI.Page
    {
        protected void Page_AbortTransaction(object sender,EventArgs e)
        {
            this.LabelAbortTransaction.Text = DateTime.Now.ToString();
        }
        protected void Page_CommitTransaction(object sender,EventArgs e)
        {
            this.LabelCommitTransaction.Text = DateTime.Now.ToString();
        }
        protected void Page_DataBinding(object sender,EventArgs e)
        {
            this.LabelDataBinding.Text = DateTime.Now.ToString();
        }
        protected void Page_Disposed(object sender,EventArgs e)
        {
            this.LabelDisposed.Text = DateTime.Now.ToString();
        }
        protected void Page_Error(object sender,EventArgs e)
        {
            Server.ClearError();
            Response.Redirect("~/ErrorLogPage.aspx");
        }
        protected void Page_Init(object sender,EventArgs e)
        {
          this.LabelInit.Text = DateTime.Now.ToString();
        }
        protected void Page_InitComplete(object sender,EventArgs e)
        {
            this.LabelInitComplete.Text = DateTime.Now.ToString();
        }
        protected void Page_Load(object sender,EventArgs e)
        {
            this.LabelLoad.Text = DateTime.Now.ToString();
        }
        protected void Page_LoadComplete(object sender,EventArgs e)
        {
            this.LabelLoadComplete.Text = DateTime.Now.ToString();
        }
        protected void Page_PreInit(object sender,EventArgs e)
        {
            this.LabelPreInit.Text = DateTime.Now.ToString();
        }
        protected void Page_PreLoad(object sender,EventArgs e)
        {
            this.LabelPreLoad.Text = DateTime.Now.ToString();
        }
        protected void Page_PreRenderComplete(object sender,EventArgs e)
        {
            this.LabelPreRenderComplete.Text = DateTime.Now.ToString();
        }
        protected void Page_PreRender(object sender,EventArgs e)
        {
            this.LabelPreRender.Text = DateTime.Now.ToString();
        }
        protected void Page_SaveStateComplete(object sender,EventArgs e)
        {
            this.LabelSaveStateComplete.Text = DateTime.Now.ToString();
        }
        protected void Page_Unload(object sender,EventArgs e)
        {
            //Page.Response.Write("");
            this.LabelUnload.Text = DateTime.Now.ToString();
        }

       
        protected void ButtonThrowException(object sender,EventArgs e)
        {
            Response.Redirect("~/ThrowExceptionPage.aspx");//Создаю необработанное исключение
        }

        protected void Button2_Click(object sender,EventArgs e)
        {
            Response.Redirect("~/Default.aspx");//На первую страницу
        }
    }
}