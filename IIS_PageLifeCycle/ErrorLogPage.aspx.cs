﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IIS_PageLifeCycle
{
    public partial class ErrorPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender,EventArgs e)
        {
            this.LabelError.Text = DateTime.Now.ToString();//Показываю время ошибки
        }

        protected void Button1_Click(object sender,EventArgs e)
        {
            Response.Redirect("~/Default.aspx");//На первую страницу
        }
    }
}