﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorLogPage.aspx.cs" Inherits="IIS_PageLifeCycle.ErrorPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
     <link href="style/style.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
    <h2>События страницы</h2>
    <table class="bordered">
        <thead>

            <tr>
                <th></th>
                <th>Имя события</th>
                <th>Время</th>
            </tr>
        </thead>
        <tr>
            <td>
                <img src="https://i-msdn.sec.s-msft.com/dynimg/IC90369.jpeg" /></td>
            <td>Error</td>
            <td><asp:Label ID="LabelError" runat="server"></asp:Label></td>
        </tr> </table>

    <br>
    <br>
        <asp:Button ID="Button1" runat="server" Text="На первую страницу" OnClick="Button1_Click" />
    </div>
    </form>
</body>
</html>
