﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IIS_PageLifeCycle
{
    public partial class VeiwState : System.Web.UI.Page
    {
        public CustomFormExample deserializedCustomForm;
        CustomFormExample deserializedLockedCustomForm;

        private string splitedString(string strViewStateValue)//Для подачи строки в удобном виде
        {
            string splitedValue = "";
            int i = 0;
            foreach ( var item in strViewStateValue )
            {
                splitedValue += item;
                i++;
                if ( i > 70 )
                {
                    i = 0;
                    splitedValue += "\n";
                }
            }
            return splitedValue;

        }

        protected void Page_Load(object sender,EventArgs e)
        {
            string keyMAC = DateTime.Now.Millisecond.ToString();


            //Создаю три экз. LosFormatter
            //Которые используется для сериализации вэб-формы
            //https://msdn.microsoft.com/en-us/library/system.web.ui.losformatter%28v=vs.110%29.aspx
            //Один без machine authentication code (MAC) key modifier
            //Второй - с ключом
            //Третий с новым, неправильным, ключом 

            System.Web.UI.LosFormatter simpleFormatter = new System.Web.UI.LosFormatter();

            System.Web.UI.LosFormatter MACFormatter = new System.Web.UI.LosFormatter(true,keyMAC);

            System.Web.UI.LosFormatter wrongMACFormatter = new System.Web.UI.LosFormatter(true,"wrongKey");


            CustomFormExample customForm = new CustomFormExample();


            StringWriter noKeyMAC = new StringWriter();
            StringWriter UsingKeyMAC = new StringWriter();

            simpleFormatter.Serialize(noKeyMAC,customForm);//Этот аналог __VIEWSTATE каждый раз на странице одинаковый
            MACFormatter.Serialize(UsingKeyMAC,customForm);//Этот аналог __VIEWSTATE всегда разный
                                                           //MACFormatter (keyMAC) - каждый раз новый

            if ( IsPostBack )
            {
                deserializedCustomForm = (CustomFormExample)simpleFormatter.Deserialize(
                this.LabelnoKeyMAC.Text.ToString());//Десериализация этого объекта пройдет без проблем
                //Без криптостойкого хеширования так же можно десериализовать и настоящий __VIEWSTATE
                //Явная угроза безопасности
                try
                {
                    deserializedLockedCustomForm = (CustomFormExample)wrongMACFormatter.Deserialize(
                  UsingKeyMAC.ToString());//Десериализация этого объекта при новом экз. wrongMACFormatter (keyMAC)
                                          //уже невозможна Validation of viewstate MAC failed. 

                }
                catch ( Exception exc )
                {
                    this.LabelDeserializedException.Text = exc.Source + " " + exc.Message + " " + exc.StackTrace;
                }



            }

            deserializedLockedCustomForm = (CustomFormExample)MACFormatter.Deserialize(UsingKeyMAC.ToString());
            //Десериализация этого объекта пройдет без проблем, т.к. при десериализации я использую
            //тот же MACFormatter с тем же ключом




            this.LabelnoKeyMAC.Text = splitedString(Server.HtmlEncode(noKeyMAC.ToString()));
            this.LabelnoKeyMACdecode.Text = System.Text.Encoding.ASCII.GetString(Convert.FromBase64String(this.LabelnoKeyMAC.Text));

            this.LabelUsingKeyMAC.Text = splitedString(Server.HtmlEncode(UsingKeyMAC.ToString()));
            this.LabelUsingKeyMACdecode.Text = System.Text.Encoding.ASCII.GetString(Convert.FromBase64String(this.LabelUsingKeyMAC.Text));

        }

        protected void Button2_Click(object sender,EventArgs e)
        {
            Response.Redirect("~/Default.aspx");//На первую страницу
        }
    }
}

//Http протокол не позволяет хранить состояние.Клиент отправил запрос,сервер ASP создал и инициализировал все объекты,создал разметку,отправил ее клиенту,уничтожил все объекты,созданные на предыдущих этапах. Новый клиентский запрос – все по-новому,по кругу. Таким образом,чтобы запоминать состояние объектов требуется какой-то механизм. Один из таких механизмов – сохранять состояние прямо на странице,в скрытом поле.
//https://msdn.microsoft.com/en-us/library/ms972976.aspx
//Пример:
//<input type = "hidden" name= "__VIEWSTATE" id= "__VIEWSTATE" value= "VyO0YMY15nX0Ok7eGzPr++u+kI+JA73DAZa4REfJYQEuMc4Enx8A2s6NsP0plw/OLr73b2fXRa2MN3U8DXT1tOYmLk4A/hkXt7i5S/73DH8=" />

//Само значение поля __VIEWSTATE (value) – это сериализированный экземпляр вэб-формы, которая вот-вот будет удалена из памяти, состояние которой требуется сохранить.Представляет собой закодированную строку.
//The view state for an ASP.NET Web page is stored,by default, as a base-64 encoded string.
//https://msdn.microsoft.com/en-us/library/system.web.ui.losformatter%28v=vs.110%29.aspx
//Получая форму от клиента,ASP сервер десериализует объекты вэб-формы из  __VIEWSTATE и инициализирует переменные. 
//Но это не объясняет,почему значения меняются каждый раз,даже если вэб-форма не изменялась от запроса к запросу.ASP сервер при сериализации должен был бы назначать тому же __VIEWSTATE то же самое значение.
//Все дело в том,что значение viewstate зашифровано. (encrypted)
//https://msdn.microsoft.com/en-us/library/system.web.ui.page.enableviewstatemac.aspx
//A view-state MAC is an encrypted version of the hidden variable that a page's view state is persisted to when the page is sent to the browser. When this property is set to true, the encrypted view state is checked to verify that it has not been tampered with on the client.
//Сделано это из соображений безопасности.Во-первых, злоумышленник мог бы раскодировать незашифрованный __VIEWSTATE и получить секретные сведения; во-вторых, злоумышленник мог бы подменить запрос от якобы этой формы и изменить свойства объектов на серверной стороне.Этому противостоит __EVENTVALIDATION,предотвращая подмену.
//https://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation%28v=vs.110%29.aspx
//Для того,что бы проверить наверняка,так это или нет,требуется отказаться от МАС шифрования.
//EnableViewStateMac=”false”,
//но,похоже,сделать это не удастся
//https://blogs.msdn.microsoft.com/webdev/2014/09/09/farewell-enableviewstatemac/
//http://forums.asp.net/t/2024888.aspx?Microsoft+now+no+longer+recongizes+enableViewStateMAC+false+How+to+Suppress+the+ViewState+MAC+Error+



