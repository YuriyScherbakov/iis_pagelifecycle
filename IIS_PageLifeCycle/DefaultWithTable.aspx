﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DefaultWithTable.aspx.cs" Inherits="IIS_PageLifeCycle.MainForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>IIS_PageLifeCycle</title>

    <link href="style/style.css" rel="stylesheet" />
</head>


<body>

    <form id="form1" runat="server">
  

    <h2>События страницы</h2>
    <table class="bordered">
        <thead>

            <tr>
                <th></th>
                <th>Имя события</th>
                <th>Время</th>
            </tr>
        </thead>
        <tr>
            <td>
                <img src="https://i-msdn.sec.s-msft.com/dynimg/IC90369.jpeg" /></td>
            <td>AbortTransaction</td>

            <td>
                <asp:Label ID="LabelAbortTransaction" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <img src="https://i-msdn.sec.s-msft.com/dynimg/IC90369.jpeg" /></td>
            <td>CommitTransaction</td>
            <td><asp:Label ID="LabelCommitTransaction" runat="server"></asp:Label></td>
        </tr>
        <tr>

            <td>
                <img src="https://i-msdn.sec.s-msft.com/dynimg/IC90369.jpeg" /></td>
            <td>DataBinding</td>
            <td><asp:Label ID="LabelDataBinding" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <img src="https://i-msdn.sec.s-msft.com/dynimg/IC90369.jpeg" /></td>
            <td>Disposed</td>
            <td><asp:Label ID="LabelDisposed" runat="server"></asp:Label></td>

        </tr>
        <tr>
            <td>
                <img src="https://i-msdn.sec.s-msft.com/dynimg/IC90369.jpeg" /></td>
            <td>Error</td>
            <td>
                <asp:Button ID="Button1" runat="server" Text="Создать ошибку" OnClick="ButtonThrowException" />
                <br>
               Не используйте в режиме Debug</td>
        </tr>
        <tr>
            <td>
                <img src="https://i-msdn.sec.s-msft.com/dynimg/IC90369.jpeg" /></td>
            <td>Init</td>

            <td><asp:Label ID="LabelInit" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <img src="https://i-msdn.sec.s-msft.com/dynimg/IC90369.jpeg" /></td>
            <td>InitComplete</td>
            <td><asp:Label ID="LabelInitComplete" runat="server"></asp:Label></td>
        </tr>
        <tr>

            <td>
                <img src="https://i-msdn.sec.s-msft.com/dynimg/IC90369.jpeg" /></td>
            <td>Load</td>
            <td><asp:Label ID="LabelLoad" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <img src="https://i-msdn.sec.s-msft.com/dynimg/IC90369.jpeg" /></td>
            <td>LoadComplete</td>

            <td><asp:Label ID="LabelLoadComplete" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <img src="https://i-msdn.sec.s-msft.com/dynimg/IC90369.jpeg" /></td>
            <td>PreInit</td>
            <td><asp:Label ID="LabelPreInit" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <img src="https://i-msdn.sec.s-msft.com/dynimg/IC90369.jpeg" /></td>
            <td>PreLoad</td>
            <td><asp:Label ID="LabelPreLoad" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <img src="https://i-msdn.sec.s-msft.com/dynimg/IC90369.jpeg" /></td>
            <td>PreRender</td>
            <td><asp:Label ID="LabelPreRender" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <img src="https://i-msdn.sec.s-msft.com/dynimg/IC90369.jpeg" /></td>
            <td>PreRenderComplete</td>
            <td><asp:Label ID="LabelPreRenderComplete" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <img src="https://i-msdn.sec.s-msft.com/dynimg/IC90369.jpeg" /></td>
            <td>SaveStateComplete</td>
            <td><asp:Label ID="LabelSaveStateComplete" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <img src="https://i-msdn.sec.s-msft.com/dynimg/IC90369.jpeg" /></td>
 <td>Unload</td>
            <td><asp:Label ID="LabelUnload" runat="server"></asp:Label></td>
        </tr>

    </table>
        <br>
        <asp:Button ID="Button2" runat="server" Text="На первую страницу" OnClick="Button2_Click" />
  </form>
    <br>
    <br>

</body>
</html>
