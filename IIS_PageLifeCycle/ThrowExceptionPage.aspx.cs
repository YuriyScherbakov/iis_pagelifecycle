﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IIS_PageLifeCycle
{
    public partial class ThrowExceptionPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender,EventArgs e)
        {
            throw new Exception();//Создаю необработанное исключение
        }
        protected void Page_Error(object sender,EventArgs e)//Обрабатываю   исключение
        {
            Server.ClearError();
            Response.Redirect("~/ErrorLogPage.aspx");//Редирект на страницу с табличкой
        }
    }
}