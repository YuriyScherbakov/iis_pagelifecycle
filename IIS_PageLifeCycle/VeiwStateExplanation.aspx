﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VeiwStateExplanation.aspx.cs" Inherits="IIS_PageLifeCycle.VeiwState" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="style/style.css" rel="stylesheet" />
    <title></title>
</head>
<body>

    <form id="form1" runat="server">


        <h2>Имитация __VIEWSTATE</h2>
        <table class="bordered">
            <thead>

                <tr>
                    <th></th>
                    <th>__VIEWSTATE</th>
                    <th>Значение</th>
                </tr>
            </thead>
            <tr>
                <td>
                   VS</td>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Криптографическая хеш-функция не применяется, значение всегда одинаковое"></asp:Label></td>

                <td>
                    <asp:Label ID="LabelnoKeyMAC" runat="server"></asp:Label></td>
            </tr>

            <tr>
                <td>VS</td>
                <td>
                    <asp:Label ID="Label3" runat="server"></asp:Label>
                </td>

                <td>
                    <asp:Label ID="LabelnoKeyMACdecode" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                   VS</td>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="Восстановленные значения класса CustomFormExample"></asp:Label>
                </td>

                <td> 
                <% if ( IsPostBack )
                    {
                        Response.Write("Actress " + this.deserializedCustomForm.actress + "<br>");
                        Response.Write("Singer " + this.deserializedCustomForm.singer + "<br>");
                        Response.Write("Terminator " + this.deserializedCustomForm.terminator + "<br>");
                        Response.Write("BankAccount " + this.deserializedCustomForm.bankAccount + "<br>");
                    }
                    %>
                </td>
            </tr>
            <tr>
                <td colspan="3"></td>
            </tr>
            <tr>
                <td>
                    VS</td>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Используется криптостойкое хеширование, значение всегда разное"></asp:Label></td>
                <td>
                    <asp:Label ID="LabelUsingKeyMAC" runat="server"></asp:Label></td>
            </tr>


            <tr>
                <td>VS</td>
                <td>
                    <asp:Label ID="Label4" runat="server"></asp:Label></td>
                <td>
                    <asp:Label ID="LabelUsingKeyMACdecode" runat="server"></asp:Label></td>
            </tr>
             <tr>
                <td>VS</td>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Попытка восстановить значения класса CustomFormExample, используя неправильный MAC ключ, вызывает ошибку"></asp:Label>
                </td>

                <td>
                    <asp:Label ID="LabelDeserializedException" runat="server"></asp:Label>
                </td>
            </tr>

        </table>
        <br>
        <asp:Button ID="Button1" runat="server" Text="Восстановить класс CustomFormExample" /><asp:Button ID="Button2" runat="server" Text="На первую страницу" OnClick="Button2_Click" />
    </form>
</body>
</html>
