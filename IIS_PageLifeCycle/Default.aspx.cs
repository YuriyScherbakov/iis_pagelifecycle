﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.Adapters;
using System.Web.UI.WebControls;

namespace IIS_PageLifeCycle
{
    public partial class Default : System.Web.UI.Page
    {
        protected override void Construct()//1
        {
           
            base.Construct();
            //this.Page.Response.Write("Construct() " + DateTime.Now.ToString() + "<br>");
            //System.NullReferenceException
            //Page пока равен null, Page.Response, соответсвенно, тоже 
        }
        protected override void AddParsedSubObject(object obj)//4 Вызывается несколько раз
        {
            base.AddParsedSubObject(obj);
            this.Page.Response.Write("AddParsedSubObject(object obj) " + DateTime.Now.ToString() + "<br>");
        }
        protected override ControlCollection CreateControlCollection()//5
        {
            this.Page.Response.Write("CreateControlCollection() " + DateTime.Now.ToString() + "<br>");
            return base.CreateControlCollection();

        }
        protected override void DataBind(bool raiseOnDataBinding)//Не вызывался, привязки не происходило
        {
            base.DataBind(raiseOnDataBinding);
            this.Page.Response.Write("DataBind(bool raiseOnDataBinding) " + DateTime.Now.ToString() + "<br>");
        }
        protected override void DataBindChildren()//Не вызывался, привязки не происходило
        {
            base.DataBindChildren();
            this.Page.Response.Write("DataBindChildren() " + DateTime.Now.ToString() + "<br>");
        }
        protected override void EnsureChildControls()//17
        {
            base.EnsureChildControls();
           
            this.Page.Response.Write("EnsureChildControls() " + DateTime.Now.ToString() + "<br>");
        }
        protected override Control FindControl(string id,int pathOffset)//Не вызывался, поиск контрола по id не производился 
        {
            this.Page.Response.Write("FindControl(string id,int pathOffset) " + DateTime.Now.ToString() + "<br>");
            return base.FindControl(id,pathOffset);
        }
        protected override void FrameworkInitialize()//2
        {
            //Объект Page создан, Page.Response создан. Метод Write у System.Web.HttpResponse можно вызывать 
            this.Page.Response.Write("FrameworkInitialize() " + DateTime.Now.ToString() + "<br>");
            base.FrameworkInitialize();
            
        }
        protected override IDictionary GetDesignModeState()//Не вызывался, не предназначен для использования из кода
        {
            this.Page.Response.Write("GetDesignModeState() " + DateTime.Now.ToString() + "<br>");
            return base.GetDesignModeState();
        }
        protected override void InitializeCulture()//3
        {
            
            base.InitializeCulture();
            this.Page.Response.Write("InitializeCulture() " + DateTime.Now.ToString() + "<br>");
        }
        protected override void InitOutputCache(int duration,string varyByContentEncoding,string varyByHeader,string varyByCustom,OutputCacheLocation location,string varyByParam)
        {//Не вызывался, не предназначен для использования из кода
            base.InitOutputCache(duration,varyByContentEncoding,varyByHeader,varyByCustom,location,varyByParam);
            this.Page.Response.Write("InitOutputCache(int duration,string varyByContentEncoding,string varyByHeader,string varyByCustom,OutputCacheLocation location,string varyByParam) " + DateTime.Now.ToString() + " < br>");
        }
        protected override void InitOutputCache(int duration,string varyByHeader,string varyByCustom,OutputCacheLocation location,string varyByParam)
        {//Не вызывался, не предназначен для использования из кода
            base.InitOutputCache(duration,varyByHeader,varyByCustom,location,varyByParam);
            this.Page.Response.Write("InitOutputCache(int duration,string varyByHeader,string varyByCustom,OutputCacheLocation location,string varyByParam) " + DateTime.Now.ToString() + " < br>");
        }
        protected override void LoadViewState(object savedState)//Не вызывался, т.к. я не записывал ничего во ViewState
            //не предназначен для использования из кода
            //Если бы я написал что-то, вроде ViewState["Count"] = 5;
            //метод был бы вызван 
        {
            base.LoadViewState(savedState);
            this.Page.Response.Write("LoadViewState(object savedState) " + DateTime.Now.ToString() + "<br>");
        }
        protected override void OnAbortTransaction(EventArgs e)//Не вызывался
        {
            base.OnAbortTransaction(e);
            this.Page.Response.Write("OnAbortTransaction(EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected override bool OnBubbleEvent(object source,EventArgs args)
        {
            this.Page.Response.Write("OnBubbleEvent(object source,EventArgs args) " + DateTime.Now.ToString() + "<br>");
            return base.OnBubbleEvent(source,args);
           
        }
        protected override void OnCommitTransaction(EventArgs e)//Не вызывался
        {
            base.OnCommitTransaction(e);
            this.Page.Response.Write("OnCommitTransaction(EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected override void OnDataBinding(EventArgs e)//Не вызывался
        {
            base.OnDataBinding(e);
            this.Page.Response.Write("OnDataBinding(EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected override void OnError(EventArgs e)
        {
            base.OnError(e);
            this.Page.Response.Write("OnError(EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected override void OnInitComplete(EventArgs e)//11
        {
            base.OnInitComplete(e);
            this.Page.Response.Write("OnInitComplete(EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected override void OnPreInit(EventArgs e)//7 Вызовет событие PreInit
        {
            base.OnPreInit(e);
            this.Page.Response.Write("OnPreInit(EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected override void OnPreLoad(EventArgs e)//13
        {
            base.OnPreLoad(e);
            this.Page.Response.Write("OnPreLoad(EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected override void OnPreRenderComplete(EventArgs e)//19
        {
            base.OnPreRenderComplete(e);
            this.Page.Response.Write("OnPreRenderComplete(EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected override void OnSaveStateComplete(EventArgs e)//22
        {
            base.OnSaveStateComplete(e);
            this.Page.Response.Write("OnSaveStateComplete(EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected override void RaisePostBackEvent(IPostBackEventHandler sourceControl,string eventArgument)
        {//Произойдет на PostBackEvent (после нажатия на кнопку) после Page_Load
            base.RaisePostBackEvent(sourceControl,eventArgument);
            this.Page.Response.Write("RaisePostBackEvent(IPostBackEventHandler sourceControl,string eventArgument) " + DateTime.Now.ToString() + "<br>");
        }
        protected override ControlAdapter ResolveAdapter()//6 Вызывается несколько раз
        {
            this.Page.Response.Write("ResolveAdapter() " + DateTime.Now.ToString() + "<br>");
            return base.ResolveAdapter();
        }
        protected override object SaveViewState()//21
        {
            this.Page.Response.Write("SaveViewState() " + DateTime.Now.ToString() + "<br>");
            return base.SaveViewState();
        }
        protected override void SetDesignModeState(IDictionary data)
        {
            this.Page.Response.Write("SetDesignModeState(IDictionary data) " + DateTime.Now.ToString() + "<br>");
            base.SetDesignModeState(data);
        }
        protected override void TrackViewState()//10
        {
            this.Page.Response.Write("TrackViewState() " + DateTime.Now.ToString() + "<br>");
            base.TrackViewState();
        }
        protected override void OnLoadComplete(EventArgs e)//15
        {
           
            base.OnLoadComplete(e);
            this.Page.Response.Write("OnLoadComplete(EventArgs e) " + DateTime.Now.ToString() + "<br>");

        }
        protected void Page_AbortTransaction(object sender,EventArgs e)//AbortTransaction == null, нет подписчиков
        {
            this.Page.Response.Write("Page_AbortTransaction(object sender,EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected void Page_CommitTransaction(object sender,EventArgs e)//CommitTransaction == null, нет подписчиков
        {
            this.Page.Response.Write("Page_CommitTransaction(object sender,EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected void Page_DataBinding(object sender,EventArgs e)//Не вызывался, привязки не происходило
        {
            this.Page.Response.Write("Page_DataBinding(object sender,EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected void Page_Disposed(object sender,EventArgs e)//Cборщик мусора уничтожает страницу, запускается событие Page.Disposed
        {                                                      //Пронаблюдать вызов метода не получилось
            this.Page.Response.Write("Page_Disposed(object sender,EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected void Page_Error(object sender,EventArgs e)//Не вызывался, ошибок не было
        {
            this.Page.Response.Write("Page_Error(object sender,EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected void Page_Init(object sender,EventArgs e)//9
        {
            this.Page.Response.Write("Page_Init(object sender,EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected void Page_InitComplete(object sender,EventArgs e)//12
        {
            this.Page.Response.Write("Page_InitComplete(object sender,EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected void Page_Load(object sender,EventArgs e)//14
        {
            this.Page.Response.Write("Page_Load(object sender,EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected void Page_LoadComplete(object sender,EventArgs e)//16
        {
            this.Page.Response.Write("Page_LoadComplete(object sender,EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected void Page_PreInit(object sender,EventArgs e)//8
        {
            this.Page.Response.Write("Page_PreInit(object sender,EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected void Page_PreLoad(object sender,EventArgs e)//14
        {
            this.Page.Response.Write("Page_PreLoad(object sender,EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected void Page_PreRenderComplete(object sender,EventArgs e)//20
        {
            this.Page.Response.Write("Page_PreRenderComplete(object sender,EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected void Page_PreRender(object sender,EventArgs e)//18
        {
            this.Page.Response.Write("Page_PreRender(object sender,EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected void Page_SaveStateComplete(object sender,EventArgs e)//23
        {
            this.Page.Response.Write("Page_SaveStateComplete(object sender,EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }
        protected void Page_Unload(object sender,EventArgs e)//24 Occurs when the server control is unloaded from memory
        {
            //"((System.Web.UI.Control)this).Page.Request" выдал исключение типа "System.Web.HttpException"
            //"В этом контексте ответ недоступен."
            //окончательная HTML-разметка уже сгенерирована и не может быть изменена
            //  this.Page.Response.Write("Page_Unload(object sender,EventArgs e) " + DateTime.Now.ToString() + "<br>");
        }

        protected void Button2_Click(object sender,EventArgs e)
        {
            Response.Redirect("~/DefaultWithTable.aspx");//Показать события в табличке
        }

        protected void Button3_Click(object sender,EventArgs e)
        {
            Response.Redirect("~/HttpRequestFormNameValueCollection.aspx");
        }

        protected void Button4_Click(object sender,EventArgs e)
        {
            Response.Redirect("~/VeiwStateExplanation.aspx");
        }
    }
}