﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="IIS_PageLifeCycle.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div><br>
        <asp:Button ID="Button1" runat="server" Text="Button For PostBack" />
        <asp:Button ID="Button2" runat="server" Text="События в табличке" OnClick="Button2_Click" />
        <asp:Button ID="Button3" runat="server" Text="Page.Request.Form в табличке" OnClick="Button3_Click" />
        <asp:Button ID="Button4" runat="server" OnClick="Button4_Click" Text="Объяснение __VIEWSTATE" />
    </div>
    </form>
</body>
</html>
