﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HttpRequestFormNameValueCollection.aspx.cs" Inherits="IIS_PageLifeCycle.HttpRequestFormNameValueCollection" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>IIS_PageLifeCycle</title>

    <link href="style/style.css" rel="stylesheet" />
</head>
<body >
    <form id="form1" runat="server">
       
        <div>
        <h2>NameValueCollection из Request.Form</h2>
        <table class="bordered">
            <thead>

                <tr style="width:100%" >
                    <th></th>
                    <th>Ключ</th>
                    <th>Значение</th>
                </tr>
            </thead>

            <%

                NameValueCollection collection;
                collection = Request.Form;


                foreach ( var value in collection.AllKeys )
                {
                    string splitedValue = "";
                    int i = 0;
                    foreach ( var item in  collection [value])
                    {
                        splitedValue += item;
                        i++;
                        if ( i > 70 )
                        {
                            i = 0;
                            splitedValue += "<br>";
                        }
                    }

                    Response.Write("<tr>" +
                "<td >" +
                    "<img src=\"Img/key-value-viewer.png\" />" +
                "</td>" +
                "<td>" + value + "</td>" +
                "<td style=\"min-width\">" + splitedValue );
                }
            %>

        </table>
        </div>
        <br>
        <asp:Button ID="Button2" runat="server" Text="На первую страницу" OnClick="Button2_Click" />
        <asp:Button ID="Button3" runat="server" Text="PostBack" /> 
        <asp:TextBox ID="TextBox1" runat="server" Text="Напишите что нибудь"></asp:TextBox>
    </form>
</body>
</html>
