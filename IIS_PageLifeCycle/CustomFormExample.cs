﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IIS_PageLifeCycle
{

    [Serializable]
    public class CustomFormExample//Предположим, мне нужно передавать эти данные от запроса к запросу.
    {                             //Храню я их на странице, как __VIEWSTATE
        //Конечно, это всего лишь имитация
        public string actress = "Sharon Stone";
        public string singer = "Britney Spears";
        public string terminator = "Arnold Schwarzenegger";

        public int bankAccount = 555;
    }
}
